﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.EmployeeModels
{
    [Serializable]
    public class Employee
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public Vacation Vacation { get; set; }

        public string FullName()
        {
            return $"{Name} {Surname}";
        }


    }
}
