﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataManager.ManagerData
{
    public class DataReader
    {
        public readonly string FileName;

        public DataReader(string fileName)
        {
            FileName = fileName;
        }

        public List<T> ReadData<T>()
        {

            using (StreamReader reader = new StreamReader(FileName))
            {
                string employeeData = reader.ReadToEnd();
                List<T> data = JsonConvert.DeserializeObject<List<T>>(employeeData);
                return data;
            }
        }

    }
}

