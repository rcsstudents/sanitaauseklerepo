﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataManager.ManagerData
{
    public class DataWriter
    {
        public readonly string FileName;

          public DataWriter (string fileName)
        {
            FileName = fileName;
        }

        public void WriteData<T>(List<T> employee)
        {
            using (StreamWriter writer = new StreamWriter(FileName))
            {
                string employeeJson = JsonConvert.SerializeObject(employee);
                writer.WriteLine(employeeJson);

            }
        }
    }
}
