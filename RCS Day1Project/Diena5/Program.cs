﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Diena5
{
    class Program
    {
        static void Main(string[] args)
        {
           /* using (StreamWriter writer = new StreamWriter("teksts.txt"))
            {
                writer.WriteLine("Pirmais teksts failā Update!");
            }
            */
            using (StreamWriter writer = File.AppendText("teksts.txt"))  
            { 
                 writer.WriteLine("Pirmais teksts failā Update!");
            }

            List<string> tekstaSaraksts = new List<string>();
            //nolasīt faila saturu
            using (StreamReader reader = new StreamReader("teksts.txt"))
            {
                string line;
                while((line=reader.ReadLine()) !=null) //pārbauda vai nākamā rindiņa nav tukša, ja nav tad iet tālāk līdz rindiņa ir tukša
                {
                    tekstaSaraksts.Add(line);
                }
            }

                foreach (string tekstaRinda in tekstaSaraksts)
            {
                Console.WriteLine(tekstaRinda);
            }

            QuizQuestion quizQuestion = new QuizQuestion();
            quizQuestion.Question = "Jautājums?";
            quizQuestion.Answer = "Atbilde";

            string json = JsonConvert.SerializeObject(quizQuestion);

            using (StreamWriter writer = new StreamWriter("MyJson.txt"))
                {
                writer.WriteLine(json);
                }
            string jsonFromFile;
            using (StreamReader reader = new StreamReader("MyJson.txt"))
            {
                jsonFromFile = reader.ReadToEnd();
            }
                QuizQuestion deserialized = JsonConvert.DeserializeObject<QuizQuestion>(jsonFromFile);

            Console.WriteLine(deserialized.Question);
            Console.WriteLine(deserialized.Answer);

            Console.ReadKey();

                //  File.WriteAllText("teksts.txt", string.Empty);    //nodzēst rindiņu failā

            
        }
    }
}
