﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena5
{
    [Serializable]  //atribūts - šo klasi mēs varēsim serializēt uz kādu datu formātu (piem json utml.)
    class QuizQuestion
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
