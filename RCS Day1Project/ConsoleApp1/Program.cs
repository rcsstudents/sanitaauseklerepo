﻿using DatuTipi;
using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyDataTypes> myDataTypesList = new List<MyDataTypes>();

            Console.WriteLine("Izpildīt programmu? (y/n)");
            while (Console.ReadLine() != "n")
            {
                MyDataTypes myDataTypes = new MyDataTypes();

                //1.Bool
                Console.WriteLine("Ievadi bool vērtību (true/false)");
                string myBoolString = Console.ReadLine();
                myDataTypes.MyBool = Convert.ToBoolean(myBoolString);

                //2.Byte
                Console.WriteLine("Ievadi byte vērtību");
                string myByte = Console.ReadLine();

                bool isConverted = byte.TryParse(myByte, out byte result);
                if (isConverted == true)
                {
                    myDataTypes.MyByte = Convert.ToByte(myByte);
                }


                //3.Short
                Console.WriteLine("Ievadi short vērtību");
                string myShort = Console.ReadLine();
                myDataTypes.MyShort = Convert.ToInt16(myShort);


                //4.Int
                Console.WriteLine("Ievadi int vērtību");
                string myInt = Console.ReadLine();
                myDataTypes.MyInt = Convert.ToInt16(myInt);

                //5.Long
                Console.WriteLine("Ievadi long vērtību");
                string myLong = Console.ReadLine();
                myDataTypes.MyLong = Convert.ToInt32(myLong);

                //6.Doyble
                Console.WriteLine("Ievadi double vērtību");
                string myDouble = Console.ReadLine();
                myDataTypes.MyDouble = Convert.ToDouble(myDouble);

                //7.Decimal
                Console.WriteLine("Ievadi decimal vērtību");
                string myDecimal = Console.ReadLine();
                myDataTypes.MyDecimal = Convert.ToDecimal(myDecimal);

                //8.String
                Console.WriteLine("Ievadi string vērtību");
                string myString = Console.ReadLine();
                myDataTypes.MyString = Convert.ToString(myString);

                //9.Char
                Console.WriteLine("Ievadi char vērtību");
                string myChar = Console.ReadLine();
                myDataTypes.MyChar = Convert.ToChar(myChar);

                myDataTypesList.Add(myDataTypes);
            }

                foreach(MyDataTypes myData in myDataTypesList)  //vai for(int i=0, i<myDataTypesList.Cpunt; i++)
                {
                 Console.WriteLine("MyBool=" +myData.MyBool);
                Console.WriteLine("MyByte=" + myData.MyByte);
                Console.WriteLine("MyShort=" + myData.MyShort);
                Console.WriteLine("MyInt=" + myData.MyInt);
                Console.WriteLine("MyLong=" + myData.MyLong);
                Console.WriteLine("MyDouble=" + myData.MyDouble);
                Console.WriteLine("MyDecimal=" + myData.MyDecimal);
                Console.WriteLine("MyString=" + myData.MyString);
                Console.WriteLine("MyChar=" + myData.MyChar);
            }

            Console.ReadKey();
        }
    }
}
