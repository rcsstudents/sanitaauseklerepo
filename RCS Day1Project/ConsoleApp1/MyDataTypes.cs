﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatuTipi
{
   public class MyDataTypes
{
    public bool MyBool { get; set;}
    public byte MyByte { get; set; }
    public short MyShort { get; set; }
    public int MyInt { get; set; }
    public long MyLong { get; set; }
    public double MyDouble { get; set; }
    public decimal MyDecimal{ get; set; }
    public string MyString { get; set; }
    public char MyChar { get; set; }
    
}
}
