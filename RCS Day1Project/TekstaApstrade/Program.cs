﻿using System;

namespace TekstaApstrade
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Ievadi vārdu");
            string burts = Console.ReadLine();

            for (int i = 0; i < burts.Length; i++)
            {
                if (burts[i] == 'a' || burts[i] == 'e' || burts[i] == 'i' || burts[i] == 'o' || burts[i] == 'u')
                {
                    Console.WriteLine("Patskaņu skaits tekstā ir: " + burts);

                    Console.ReadKey();
                }
            }
        }
    }
}