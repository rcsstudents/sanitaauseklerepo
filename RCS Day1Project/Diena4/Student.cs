﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena4
{
    public class Student : Person, IPerson
    {
        public Student(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public string GetHomework()
        {
            string fullName = GetFullName();
            return $"Student {fullName} has a lot of homework!";
        }
    }
}
