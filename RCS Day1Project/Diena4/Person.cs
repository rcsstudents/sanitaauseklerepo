﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena4
{
    public abstract class Person
    {
        public string Name;
        public string Surname;

        public string GetFullName()
        {
            return $"{Name} {Surname}";
        }

        public virtual void GetSomething()
        {
            Console.WriteLine("I dont drink food");
        }
    }
     
}
    
   

