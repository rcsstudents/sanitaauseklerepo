﻿using System;
using System.Collections.Generic;

namespace Diena4
{
    class Program
    {
        static void Main(string[] args)
           
        {
            List<IPerson> persons = new List<IPerson>();
            persons.Add(new Student("Sandis", "Ozols"));
            persons.Add(new Professor("Juris", "Ābols"));

            foreach(IPerson person in persons)
            {
                Console.WriteLine(person.GetHomework());
            }

            Console.ReadKey();
        }
    }
}
