﻿using System;
using System.Collections.Generic;

namespace MajasDarbi
{
    class Program
    {
        static void Main(string[] args)
        {
            string mala1, mala2, mala3, figura1, figura2;
            int variants = 0;

            List<string> figuras = new List<string>();

            bool iziet = false;
            while (!iziet)
            {
                Console.Clear();
                Console.WriteLine("Kādai figūrai vēlies aprēķināt perimetru un laukumu? (1 vai 2)");
                Console.WriteLine("1 - Taisnstūris");
                Console.WriteLine("2 - Trīsstūris");
                string variantsT = Console.ReadLine();
                variants = Convert.ToInt32(variantsT);
                switch (variants)
                {
                    case 1:
                        Console.WriteLine("Ievadi 1 malas garumu: ");
                        mala1 = Console.ReadLine();
                        double mala1t = Convert.ToDouble(mala1);
                        Console.WriteLine("Ievadi 2 malas garumu: ");
                        mala2 = Console.ReadLine();
                        double mala2t = Convert.ToDouble(mala2);
                        Taisnsturis t = new Taisnsturis(mala1t, mala2t);

                        Console.WriteLine("Taisnstūra perimetrs = " + t.Perimetrs());
                        Console.WriteLine("Taisnstūra laukums = " + t.Laukums() + "\n");

                        figura1 = "Ja taisnstūra malu garums ir " + mala1t + "cm un " + mala2t + "cm, " +
                        "tad šāda taisnstūra perimetrs ir " + t.Perimetrs() + "cm, bet laukums ir " + t.Laukums() + "kv.cm.";
                        figuras.Add(figura1);
                        break;

                    case 2:
                        Console.WriteLine("Ievadi 1 malas garumu: ");
                        mala1 = Console.ReadLine();
                        double mala1tr = Convert.ToDouble(mala1);
                        Console.WriteLine("Ievadi 2 malas garumu: ");
                        mala2 = Console.ReadLine();
                        double mala2tr = Convert.ToDouble(mala2);
                        Console.WriteLine("Ievadi 3 malas garumu: ");
                        mala3 = Console.ReadLine();
                        double mala3tr = Convert.ToDouble(mala3);
                        Trissturis tr = new Trissturis(mala1tr, mala2tr, mala3tr);

                        Console.WriteLine("Trīsstūra perimetrs = " + tr.Perimetrs().ToString());
                        Console.WriteLine("Trīsstūra laukums = " + tr.Laukums() + "\n");

                        figura2 = "Ja trīsstūra malu garums ir " + mala1tr + "cm, " + mala2tr + "cm, un " + mala3tr +
                        "cm, tad šāda trīsstūra perimetrs ir " + tr.Perimetrs() + "cm, bet laukums ir " + tr.Laukums() + "kv.cm.";
                        figuras.Add(figura2);
                        break;
                }
                Console.WriteLine("Vai gribi turpināt? (y / n)");
                string choice = Console.ReadLine();
                if (choice == "n")
                {
                    iziet = true;
                }
            }
            foreach (string fig in figuras)
            {
                Console.WriteLine(fig);
            }
            Console.ReadLine();
        }
    }
}
            string mala1, mala2, mala3, figura1, figura2;
        int variants = 0;

        List<string> figuras = new List<string>();

        bool iziet = false;
            while (!iziet)
            {
                Console.Clear();
                Console.WriteLine("Kādai figūrai vēlies aprēķināt perimetru un laukumu? (1 vai 2)");
                Console.WriteLine("1 - Taisnstūris");
                Console.WriteLine("2 - Trīsstūris");
                string variantsT = Console.ReadLine();
        variants = Convert.ToInt32(variantsT);
                switch (variants)
                {
                    case 1:
                        Console.WriteLine("Ievadi 1 malas garumu: ");
                        mala1 = Console.ReadLine();
                        double mala1t = Convert.ToDouble(mala1);
        Console.WriteLine("Ievadi 2 malas garumu: ");
                        mala2 = Console.ReadLine();
                        double mala2t = Convert.ToDouble(mala2);
        Taisnsturis t = new Taisnsturis(mala1t, mala2t);

        Console.WriteLine("Taisnstūra perimetrs = " + t.Perimetrs());
                        Console.WriteLine("Taisnstūra laukums = " + t.Laukums() + "\n");

                        figura1 = "Ja taisnstūra malu garums ir " + mala1t + "cm un " + mala2t + "cm, " +
                        "tad šāda taisnstūra perimetrs ir " + t.Perimetrs() + "cm, bet laukums ir " + t.Laukums() + "kv.cm.";
                        figuras.Add(figura1);
                        break;

                    case 2:
                        Console.WriteLine("Ievadi 1 malas garumu: ");
                        mala1 = Console.ReadLine();
                        double mala1tr = Convert.ToDouble(mala1);
        Console.WriteLine("Ievadi 2 malas garumu: ");
                        mala2 = Console.ReadLine();
                        double mala2tr = Convert.ToDouble(mala2);
        Console.WriteLine("Ievadi 3 malas garumu: ");
                        mala3 = Console.ReadLine();
                        double mala3tr = Convert.ToDouble(mala3);
        Trissturis tr = new Trissturis(mala1tr, mala2tr, mala3tr);

        Console.WriteLine("Trīsstūra perimetrs = " + tr.Perimetrs().ToString());
                        Console.WriteLine("Trīsstūra laukums = " + tr.Laukums() + "\n");

                        figura2 = "Ja trīsstūra malu garums ir " + mala1tr + "cm, " + mala2tr + "cm, un " + mala3tr +
                        "cm, tad šāda trīsstūra perimetrs ir " + tr.Perimetrs() + "cm, bet laukums ir " + tr.Laukums() + "kv.cm.";
                        figuras.Add(figura2);
                        break;
                }
    Console.WriteLine("Vai gribi turpināt? (y / n)");
                string choice = Console.ReadLine();
                if (choice == "n")
                {
                    iziet = true;
                }
            }
            foreach (string fig in figuras)
            {
                Console.WriteLine(fig);
            }
            Console.ReadLine();
        }
    }
}