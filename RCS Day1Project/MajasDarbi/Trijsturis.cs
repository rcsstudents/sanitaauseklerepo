﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MajasDarbi
{
     public class Trijsturis
    {
        double mala1, mala2, mala3;

        public Trijsturis(double mala1, double mala2, double mala3)
        {
            this.mala1 = mala1;
            this.mala2 = mala2;
            this.mala3 = mala3;
        }

        public double Perimetrs()
        {
            return mala1 + mala2 + mala3;
        }
        public double Laukums()
        {
            double p = Perimetrs() / 2;
            return Math.Sqrt(p * (p - mala1) * (p - mala2) * (p - mala3));
        }


    }
}

