﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena3
{
   public class Person : Food, IWalking
    {
        public string Name;
        public double Height;
        public double Weight;
        public string HairColor;
        public DayOfWeekEnum FavoriteWeekDay;
        private DayOfWeekEnum monday;

        public Person(string name, double height, double weight, string hairColor, DayOfWeek favoriteWeekDay)
        { 
            Name = name;
            Height = height;
            Weight = weight;
            HairColor=hairColor;
            FavoriteWeekDay = favoriteWeekDay;
         }

        public Person(string name, double height, double weight, string hairColor, DayOfWeekEnum monday)
        {
            Name = name;
            Height = height;
            Weight = weight;
            HairColor = hairColor;
            this.monday = monday;
        }

        public virtual void WritePersonInfo()
        {
            Console.WriteLine($"Persona{Name} ir {Height} cm augumā un sver {Weight} kg");
            Console.WriteLine($"Matu krāsa ir {HairColor}");
            Console.WriteLine($"Mīļākā nedēļas diena ir {FavoriteWeekDay}");
        }

        public void DoWalking()
        {
            Console.WriteLine("Persona nogāja 10 km");
        }
        public override void Drink()
        {
            Console.WriteLine("Person drinks beer");
        }        
    }
}
