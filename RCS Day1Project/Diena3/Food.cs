﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena3
{
    public abstract class Food
    {
        
        //public abstract void DrinkAbstract()       //nesatur funkcionalitāti
                                                //obligāti vajag override šo metodi

         public virtual void Drink()            //satur funkcionaitāti
                                                //var override šo metodi, ja nepieciešams
        {
            Console.WriteLine("I drink beer");
        }
      
        public void Eat()                   //satur funkciinalitāti
                                            //nevar override

        {
            Console.WriteLine("I like beer");
        }
    }
}
