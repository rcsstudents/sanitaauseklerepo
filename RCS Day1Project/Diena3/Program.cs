﻿using System;
using System.Collections.Generic;

namespace Diena3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Diena3():
            Animal animal = new Animal();
            Person person = new Person("Lauris", 123, 123, "blue", DayOfWeek.Friday);

            animal.Eat();
            animal.Drink();

            Console.ReadKey();
        }

        public static void Diena3()
        {
            /*
Console.WriteLine("Ievadi skaitli no 1-7");
string nedelasDiena = Console.ReadLine();

/*

int nedelasDienasNumurs = Convert.ToInt32(nedelasDiena);
Console.ReadKey();

switch (nedelasDienasNumurs)
{
    case (int)DayOfWeekEnum.Monday:
        Console.WriteLine("Šī ir pimdiena");
    break;
    default:
        Console.WriteLine("Diena netika atrasta");
    break;

   */

            /*
            string diena = Console.ReadLine();
            int dienaNumurs = Convert.ToInt32(diena);

            string menesis = Console.ReadLine();
            int menesisNumurs = Convert.ToInt32(menesis);

            string gads = Console.ReadLine();
            int gadsNumurs = Convert.ToInt32(gads);

            //1.lietotājs ievada datumu
            //int diena = 1, menesis = 2, gads = 2000;

            //2. Izveido datuma objektu
            DateTime datums = new DateTime(gadsNumurs, menesisNumurs, dienaNumurs);

            //3. Nolasīt DayOfWeek no datuma
            int dayOfWeek = (int)datums.DayOfWeek;

            //4.
            switch(dayOfWeek)
            {
                case (int)DayOfWeek.Monday:
                    Console.WriteLine("Šī ir pirmdiena");
                    break;
                default:
                    Console.WriteLine("Diena netika atrasta");
                    break;
            }

            if (dayOfWeek == (int)DayOfWeek.Saturday || dayOfWeek == (int)DayOfWeek.Sunday)
            {
                Console.WriteLine("Šī ir brīvdiena");
            }
              else
              {
                Console.WriteLine("Šī ir darba diena");
               }
            Console.ReadKey();
        }
        */
            List<IWalking> walkingList = new List<IWalking>();
            List<Person> PersonList = new List<Person>();
            List<Programmer> ProgrammerList = new List<Programmer>();

            string turpinat = "";
            while (turpinat != "n")
            {

                Console.WriteLine("Ievadi vārdu");
                string vards = Console.ReadLine();

                Console.WriteLine("Ievadi augumu");
                string augums = Console.ReadLine();
                double augumsDouble = Convert.ToDouble(augums);

                Console.WriteLine("Ievadi svaru");
                string svars = Console.ReadLine();
                double svarsDouble = Convert.ToDouble(svars);

                Console.WriteLine("Ievadi matu krāsu");
                string matuKrasa = Console.ReadLine();

                Console.WriteLine("Ievadi dienu 1-7");
                string dayOfWeek = Console.ReadLine();
                int dayOfWeekInt = Convert.ToInt32(dayOfWeek);
                DayOfWeekEnum nedelasDiena = (DayOfWeekEnum)dayOfWeekInt;

                Person cilveks = new Person(vards, augumsDouble, svarsDouble, matuKrasa, nedelasDiena);
                PersonList.Add(cilveks);

                Programmer programmer = new Programmer(vards, augumsDouble, svarsDouble, matuKrasa, "C#");
                programmerList.Add(programmer);

                Animal animal = new Animal();
                animal.Eat();
                animal.Drink();

                walkingList.Add(cilveks);
                walkingList.Add(animal);


                Console.WriteLine("Turpināt? y/n");
                turpinat = Console.ReadLine();
            }

                foreach (Person person in PersonList)
                {
                   person.WritePersonInfo();
                }
                foreach (Programmer person in ProgrammerList)
                {
                  programmer.WritePersonInfo();
                }
                foreach (IWalking walking in walkingList)
                {
                    walking.DoWalking();
                }

                Console.ReadKey();

        }

       
    }

}

          