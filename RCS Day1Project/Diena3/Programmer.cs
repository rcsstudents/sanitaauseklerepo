﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diena3
{
    public class Programmer : Person
    {
        public string ProgrammingLanguage;

        public Programmer(string name, double height, double weight, string hairColor, string programmingLanguage) :

         base(name, height, weight, hairColor, DayOfWeekEnum.Monday)
        {
            ProgrammingLanguage = programmingLanguage;
        }

        public override void WritePersonInfo()
        {
            Console.WriteLine($"Programmētājs {Name} ir {Height} cm augumā un sver {Weight} kg");
            Console.WriteLine($"Matu krāsa ir {HairColor}");
            Console.WriteLine($"Programmēšanas valoda ir {ProgrammingLanguage}");
        }
    }
}


