﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniversityManagement
{
    public enum DataAction
    {
        Create = 0,
        Read = 1,
        Update = 2,
        Delete = 3

    }
}
