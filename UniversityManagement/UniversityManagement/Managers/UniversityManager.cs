﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniversityManagement.Managers
{
    public class UniversityManager : IManager
    {
        public void Create()
        {
            Console.WriteLine("UniversityManager - Create()");
        }

        public void Delete()
        {
            Console.WriteLine("UniversityManager - Read()");
        }

        public void Read()
        {
            Console.WriteLine("UniversityManager - Update()");
        }

        public void Update()
        {
            Console.WriteLine("UniversityManager - Delete()");
        }
    }
}
