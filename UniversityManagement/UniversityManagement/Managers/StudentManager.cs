﻿using DataManager;
using DataManager.ManagerData;
using System;
using System.Collections.Generic;
using System.Text;

namespace UniversityManagement.Managers
{
    public class StudentManager: IManager
    {
        List<StudentManager> Students;
        DataReader reader;
        DataWriter writer;

        public StudentManager()
        {
            reader = new DataReader(DataConfiguration.StudentData);
            writer = new DataWriter(DataConfiguration.StudentData);
            Students = reader.ReadData<StudentManager>();
        }

        public void Create()
        {
            Console.WriteLine("StudentManager - Read()");
        }

        public void Delete()
        {
            Console.WriteLine("StudentManager - Create()");
        }

        public void Read()
        {
            Console.WriteLine("StudentManager - Update()");
        }

        public void Update()
        {
            Console.WriteLine("StudentManager - Delete()");
        }
    }
}
