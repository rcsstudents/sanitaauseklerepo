﻿using System;
using UniversityManagement;
using UniversityManagement.Managers;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Wlcome to UniversityManagement!");
                Console.WriteLine("Enter 0 = (student) or 1 = )University):");
                string entityTypeString = Console.ReadLine();
                int entityType = Convert.ToInt32(entityTypeString);

                IManager manager = GetEntityManager((EntityType)entityType);

                DoDataActions(manager);
            }
        }



        public static IManager GetEntityManager(EntityType entityType)
        {
            IManager manager = null;
            switch (entityType)
            {
                case EntityType.Student:
                    manager = new StudentManager();
                    break;
                case EntityType.University:
                    manager = new UniversityManager();
                    break;
                default:
                    Console.WriteLine($"Cannot find entity with id={(int)entityType}");
                    break;
            }
            return manager;
        }
        private static void DoDataActions(IManager manager)
        {
            Console.WriteLine("Enter 0 =(create) or 1=(Read) or 2=(Update) or 3=(Delete):");
            string dataActionString = Console.ReadLine();
            int dataAction = Convert.ToInt32(dataActionString);

            switch ((DataAction)dataAction)
            {
                case DataAction.Create:
                    manager.Create();
                    break;
                case DataAction.Read:
                    manager.Read();
                    break;
                case DataAction.Update:
                    manager.Update();
                    break;
                case DataAction.Delete:
                    manager.Delete();
                    break;
                default:
                    Console.WriteLine($"Invalid data action:{dataAction}");
                    break;

            }

        }
    }
}
