﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums1
{
    class Program
    {
        static void Main(string[] args)
        {

            List<int> saraksts = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            
                        //1 Nepara skaitli
            NeparaSkaitli(saraksts);
            
                        //2 Pāra skaitļi
            ParaSkaitli(saraksts);
            
                        //3 Dalas ar 3
            DalasAr3(saraksts);
            
                        //4 Lielaki2Mazaki8
            Lielaki2Mazaki8(saraksts);
            
                        //5 >=4 <= 9
            LielakiVienadi4MazakiVienadi9(saraksts);
                    }

        private static void LielakiVienadi4MazakiVienadi9(List<int> saraksts)
        {
            Console.WriteLine("5. Lielaki vienādi ar 4 mazaki vienādi ar 9");
            saraksts
                .Where(s => s >= 4 && s <= 9)
                .ToList()
                .ForEach(s => Console.WriteLine(s));
        }

        private static void Lielaki2Mazaki8(List<int> saraksts)
        {
            Console.WriteLine("4. Lielaki par 2 mazaki par 8");
            saraksts
                .Where(s => s > 2 && s< 8)
                .ToList()
                .ForEach(s => Console.WriteLine(s));
        }

        private static void DalasAr3(List<int> saraksts)
        {
            Console.WriteLine("3. Dalas ar 3");
            saraksts
                .Where(s => s % 3 == 0 && s != 0)
                .ToList()
                .ForEach(s => Console.WriteLine(s));
        }

        private static void ParaSkaitli(List<int> saraksts)
        {
            Console.WriteLine("2. Para skaitļi");
            saraksts
                .Where(s => s % 2 == 0)
                .ToList()
                .ForEach(s => Console.WriteLine(s));
        }

        public static void NeparaSkaitli(List<int> saraksts)
        {
            //List<int> resultList = new List<int>();
            //foreach(int s in saraksts)
            //{
            //    if(s % 2 != 0)
            //    {
            //        resultList.Add(s);
            //    }
            //    if (s % 2 == 0)
            //    {
            //        resultList.Add(s);
            //    }
            //}

            //foreach (int result in resultList)
            //{
            //    Console.WriteLine(result);
            //}


            //List<int> neparaSkaitli = saraksts.Where(s => s % 2 != 0).ToList();
            //foreach(int skaitlis in neparaSkaitli)
            //{
            //    Console.WriteLine(skaitlis);
            //}

            Console.WriteLine("1. Nepara skaitli");
            saraksts
                .Where(s => s % 2 != 0)
                .ToList()
                .ForEach(s => Console.WriteLine(s));
        }
    }
}