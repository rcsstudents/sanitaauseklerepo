﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<int> saraksts = new List<int> { 3, 9, 2, 8, 6, 5 };
            saraksts
                .Select(s=> $"{s} * {s} = { s* s}")
                .ToList()
                .ForEach(s => Console.WriteLine(s));

            Console.WriteLine("Saraksta summa = ");
            int summa = saraksts.Sum();
            Console.WriteLine(summa);

            Console.ReadKey();
        }
    }
}
