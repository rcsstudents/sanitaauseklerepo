﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums8
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> saraksts = new List<string>
            {
                "vards1", "vards2", "vards3", "kaķis", "suns", "pele"
            };
            
            string sarakstaElementi = string.Join(">*<", saraksts);
            Console.WriteLine($"<{sarakstaElementi}>");
         }
    }
}