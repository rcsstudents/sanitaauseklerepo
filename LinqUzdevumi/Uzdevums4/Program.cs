﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1. Ievadiet vārdu");
            string value = Console.ReadLine().ToLower();
            
            value
                            .GroupBy(s => s)
                            .Select(s => $"Burts - '{s.Key}' atkārtojas '{s.Count()}' reizes")
                            .ToList()
                            .ForEach(s => Console.WriteLine(s));
            
            Console.WriteLine("2. Vārda patskaņi");
            List<string> patskani = new List<string> { "a", "e", "i", "u", "o" };
            value
                            .Where(v => patskani.Contains(v.ToString()))
                            .GroupBy(s => s)
                            .Select(s => $"Patskanis - '{s.Key}' atkārtojas '{s.Count()}' reizes")
                            .ToList()
                            .ForEach(v => Console.WriteLine(v));
            
            Console.WriteLine("3. Vārda līdzskaņi");
            value
                            .Where(v => !patskani.Contains(v.ToString()))
                            .GroupBy(s => s)
                            .Select(s => $"Līdzskanis - '{s.Key}' atkārtojas '{s.Count()}' reizes")
                            .ToList()
                            .ForEach(v => Console.WriteLine(v));
            
            Console.ReadKey();
                    }
    }
}
