﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums7
{
    internal class Program
    {
        internal static void Main(string[] args)
        {

            Console.WriteLine("Ievadet vārda virkni: ");
            string varduVirkne = Console.ReadLine();
            List<string> vardi = varduVirkne.Split(',').ToList();

            //1.Izdrukāt uz ekrāna visus vārdu, kas sākās ar lielo burtu:

            Console.WriteLine("2.Lielie burti sākums");
            vardi
                .Where(v => v[0].ToString().Equals(v[0].ToString().ToUpper()))
                 .ToList()
                .ForEach(v => Console.WriteLine(v));

            LielieBurtiSakums(vardi);

            //3.Izdrukāt uz ekrāna visus vārdus, kas beidzās ar lielo burtu:

            LielieBurtiBeigas(vardi);


            //4.Izdrukāt uz ekrāna visus vārdus, kas beidzās ar mazo burtu:

            Console.WriteLine("5.Lielie burti beigās");
            vardi
               .Where(v => v.Last().ToString().Equals(v.Last().ToString().ToLower()))
                .ToList()
               .ForEach(v => Console.WriteLine(v));

            Console.ReadKey();
        }

        private static void LielieBurtiBeigas(List<string> vardi)
        {
            Console.WriteLine("4.Lielie burti beigās");
            vardi
               .Where(v => v.Last().ToString().Equals(v.Last().ToString().ToUpper()))
                .ToList()
               .ForEach(v => Console.WriteLine(v));
        }

        private static void LielieBurtiSakums(List<string> vardi)
        {
            //2.Izdrukāt uz ekrāna visus vārdus, kas sākās ar mazo burtu:

            Console.WriteLine("3.Mazie burti sākums");
            vardi
               .Where(v => v.First().ToString().Equals(v.Last().ToString().ToLower()))
                .ToList()
               .ForEach(v => Console.WriteLine(v));
        }
    }
}
