﻿using System;
using System.Collections.Generic;

namespace Uzdevums10
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            List<string> saraksts = new List<string>
            {
               "vertiba1", "vertiba2", "vertibaaa"
            };

            string joined = GetJoinedString(saraksts);

            Console.WriteLine(joined);

            Console.WriteLine("Izvēlieties kādu no šiem vārdiem");
            string vards = Console.ReadLine();

            if (!saraksts.Contains(vards))
            {
                Console.WriteLine($"Whoooops, this word {vards} was not found");
                return;
            }

            saraksts.Remove(vards);

            string joinedNew = GetJoinedString(saraksts);
            Console.WriteLine(joinedNew);

            Console.ReadKey();
        }

        private static string GetJoinedString(List<string> saraksts)
        {
            return "<" + string.Join("><", saraksts) + ">";
        }
    }
    
}
