﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums5
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            List<string> saraksts = new List<string>
             {
                "Rīga",
                "Liepāja",
                "Valmiera",
                "Rēzekne",
                "Jūrmala",
                "Jēkabpils",
                "Jaunpiebalga",
                "Smiltene",
                "Roja",
                "Ventspils"
            };
            
            Console.WriteLine("Ievadiet burtu:");
            string sakums = Console.ReadLine();
            Console.WriteLine($"Pilsētas, kuras sākas ar burtu {sakums}");
            saraksts
                            .Where(s => s.ToLower()
                                .StartsWith(sakums))
                            .ToList()
                            .ForEach(s => Console.WriteLine(s));
            
            Console.WriteLine("Ievadiet burtu:");
            string beigas = Console.ReadLine();
            Console.WriteLine($"Pilsētas, kuras sākas ar burtu {beigas}");
            saraksts
                            .Where(s => s.ToLower()
                                .EndsWith(beigas))
                            .ToList()
                            .ForEach(s => Console.WriteLine(s));
                    }
    }
}