﻿using System;
using System.Collections.Generic;
using System.Linq;
using Uzdevums9;

namespace Uzdevums11
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>();
            Uzdevums9.Program.AddStudents(students);
            WriteStudentInfo(students);

            Console.WriteLine("Ievadi kāda studenta Id vērtību");
            int id = Convert.ToInt32(Console.ReadLine());

            Student studentToBeRemoved = students.Where(s => s.Id == id).FirstOrDefault();
            if (studentToBeRemoved == null)
            {
                Console.WriteLine($"Students ar id={id} netika atrasts");
                return;
            }

            students.Remove(studentToBeRemoved);
            WriteStudentInfo(students);

            Console.ReadKey();
        }

        private static void WriteStudentInfo(List<Student> students)
        {
            students.ForEach(s => Console.WriteLine($"Id={s.Id} Name={s.Name} Grade={s.Grade}"));
        }
    
    }
}
