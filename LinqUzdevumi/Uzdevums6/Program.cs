﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums6
{
    static class Program
    {
        static void Main(string[] args)
        {
            List<int> saraksts = new List<int>();

            for (int i = 1; i <= 100; i++)
            {
                saraksts.Add(i);
            }

            //saraksts.Shuffle();

            saraksts.TakeLast(10).ToList().ForEach(s => Console.WriteLine(s));

            Console.ReadKey();
        }

    public static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            Console.ReadKey();
        }
        
    }
}



