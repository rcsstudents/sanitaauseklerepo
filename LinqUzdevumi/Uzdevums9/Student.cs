﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uzdevums9
{
   public class Student
    {
        public int Id;
        public string Name;
        public byte Grade;

        //1.Uzģenerēt konstruktoru, kas aizpilda šīs vērtības

        public Student(int id, string name, byte grade)
        {
            Id = id;
            Name = name;
            Grade = grade;
        }
    }
}
