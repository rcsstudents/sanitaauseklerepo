﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums9
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>();
            AddStudents(students);
            
            AugstakaAtzimeStudentiem(students);
            ZemakaAtzimeStudentiem(students);
            
            Console.WriteLine();
                    }

        private static void ZemakaAtzimeStudentiem(List<Student> students)
        {
            Student lowestGradeStudent =
              students.OrderBy(s => s.Grade).First();

            int zemakaAtzime = lowestGradeStudent.Grade;

            students.Where(s => s.Grade == zemakaAtzime)
                .ToList()
                .ForEach(s => Console.WriteLine($"Zemākā atzīme {s.Grade} " +
                $"ir studentam {s.Name} ar Id={s.Id}"));
        }

        private static void AugstakaAtzimeStudentiem(List<Student> students)
        {
            Student highGradeStudent =
               students.OrderByDescending(s => s.Grade).First();

            int augstakaAtzime = highGradeStudent.Grade;

            students.Where(s => s.Grade == augstakaAtzime)
                .ToList()
                .ForEach(s => Console.WriteLine($"Augstākā atzīme {s.Grade} " +
                $"ir studentam {s.Name} ar Id={s.Id}"));
        }

        private static void AddStudents(List<Student> students)
        {
            students.Add(new Student(1, "Lauris", 10));
            students.Add(new Student(2, "Jānis", 2));
            students.Add(new Student(3, "Toms", 6));
            students.Add(new Student(4, "Kārlis", 9));
            students.Add(new Student(5, "Dāvis", 2));
        }
    }
}