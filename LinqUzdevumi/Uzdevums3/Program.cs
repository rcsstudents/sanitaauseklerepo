﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uzdevums3
{
    public class Program
    {
       public static void Main(string[] args)
        {
            List<int> saraksts = new List<int> { 5, 9, 1, 2, 3, 7, 5, 6, 7, 3, 7, 6, 8, 5, 4, 9, 6, 2 };

            saraksts
                .GroupBy(s => s)
                .Select(s => $"Elements:{s.Key} atkārtojas {s.Count()}reizes")
                .ToList()
                .ForEach(s => Console.WriteLine(s));

           //List<string> groupedSaraksts = saraksts.GroupBy(s => s).Select(s => $"{s.Key}{s.Count()}").ToList();
          // groupedSaraksts.ForEach(g => Console.WriteLine(g));
          Console.ReadKey();
        }
    }
}
